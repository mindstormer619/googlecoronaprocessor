# Google Coronavirus Data Processor

**2020-04-28**

Just a simple Python notebook to process Google's coronavirus data from https://news.google.com/covid19/map and get simple useful statistics.

Please note that the format of the source data is not controlled by me and can be changed at any time by Google. The script may not be up-to-date.

## Steps

1. Open the link, copy the data and store it into the `raw` string. It should look similar to the data that's already present.
2. Run all cells.
3. You can add new cells with your own fields or data processing and run them 😊
